package br.com.devdojo.projetoinicial.persistence.model.enums;

/**
 * Created by William Suane on 11/9/2016.
 */
public enum Condition {
    LIKE("LIKE"), EQUAL("=");
    private String condition;

    Condition(String condition) {
        this.condition = condition;
    }

    public String getCondition() {
        return condition;
    }
}
