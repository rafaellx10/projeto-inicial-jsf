Projeto desenvolvido com o intuito de ser utilizado como base para outros projetos.
Tecnologias utilizadas inicialmente (que podem ser alteradas como IDE, banco etc): 
* IntelliJ como IDE, 
* Wildfly 10.1 como container EE, 
* Hibernate 5.0.10 e JPA 2.1 (versões contidas no wildfly) e c3p0 como pool de conexões, 
* primefaces 6.0, 
* JSF 2.2, 
* Maven, 
* Ant, 
* MySQL como banco de dados.

O tutorial utilizado para criar esse projeto pode ser encontrado neste link: http://www.devdojo.com.br/productionmode.php

Existe espaço para melhorias de código e adição de novas funcionalidades, se fizermos alguma modificação será gravada uma aula sobre ela.

Para saber das novidades sobre os cursos: https://www.facebook.com/devdojobr/

Sinta-se livre para utilizar este projeto em qualquer projeto que você quiser, comercialmente ou não. Pedimos apenas que não copie o código e divulgue utilizando seu nome.